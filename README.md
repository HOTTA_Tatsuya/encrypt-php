# エンコードとデコード

## 使い方

- BASHで`$ php -S localhost:8000`
- `localhost:8000/enc.php?data=test`にアクセス
    - "test"がエンコードされて"bI5lwbjheq6GEhj2c4HSgA=="になる
- `localhost:8000/index.php?data=bI5lwbjheq6GEhj2c4HSgA==`にアクセス
    - デコードされて"test"に戻る    

## 仕様

- `openssl_decrypt()`及び`openssl_encrypt()`を使用。
    - [マニュアル1](https://www.php.net/manual/ja/function.openssl-decrypt.php)
    - [マニュアル2](https://www.php.net/manual/ja/function.openssl-encrypt.php)
